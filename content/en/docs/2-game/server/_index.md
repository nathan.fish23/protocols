 ---
title: "Server"
date: 2020-02-16
weight: 30
---
{{< pg_start >}}

In the previous protocol, the problem was that even the secret was encrypted, the key was sent in plaintext! 

We now assume the existence of a server S, which can securely communicate with A using the key KAS, and with B using the key KBS. We also assume that S can communicate with E using the key KES. This is an important assumption, as it implies that the attacker E is somehow registered with S.

In the protocol below, since S can communicate with B using KBS, A simply asks S to send them the key KBS, encrypted by KAS. The previous attack does not work, since no key is sent in plaintext. 

<b>Knowledge:</b> A:[s, KAS, A, B], B:[KBS, A, B], S:[A, B, E, KAS, KBS, KES] E:[A, B, KES, fake]<br>
<b>Steps:</b>
  <ol>
    <li> A -> S: A, B
    <li> S -> A: {KBS}KAS
    <li> A -> B: {#s}KBS
  </ol>
	  

	  
{{< protocolgame >}}

<script>	  
 class Sender extends Agent {
	      
	      constructor(id, facts, network) {
		  super(id, facts, network);
		  this.session = 0;
		  this.state = 0;
	      }
	      
	      // Sender has the following states
	      // init: initial state, ready to send message 1
	      // (in case of multiple sessions, message 3 from previous session
	      // has been sent)
	      // waiting_2: message 1 sent, waiting for message 2. 
	      
	      process (msg, step) {
		  switch (this.state) {
		  case 0:
		      this.init(step); break;
		  case 1:
		      this.sendEncryptedSecret(msg, step); break;
		  default:
		      throw 'Unknown state for agent ' + this.id;
		  }
	      }
	      
	      getSessionSecret (step) {
		  var secret = 's_' + this.session;
		  this.learns(secret, step)
		  return secret;
	      }
	      
	      sendEncryptedSecret(msg, step) {
		  var facts = msg.content.getFacts();
		  if (facts.length != 1) { 
		      console.log('Expecting exactly one argument, received: ' + facts.length);
		      return;
		  }
		  
		  var KBS_KAS = facts[0];
		  this.learns(KBS_KAS, step);

		  var KBS = this.decrypt(KBS_KAS, 'KAS', step);
		  
		  var new_msg = {source:this.id, destination:'B',
				 content:this.encrypt(this.getSessionSecret(step), KBS, step)};
		  this.sendMessage(new_msg, step);
		  this.state = 0;
		  
	      }
	      
	      init (step) {
		  var msg = {source:this.id, destination:'S', content:'A, B'}; 
		  this.sendMessage(msg, step);
		  this.session++;
		  this.state = 1;
	      }
	  }

	  class Receiver extends Agent {
	      constructor(id, facts, network) {
		  super(id, facts, network);
		  this.state = 0;
	      }

	      process(msg, step) {
		  switch (this.state) {
		  case 0:
		      this.decryptMessage(msg, step); break;
		  default:
		      throw 'Unknown state for agent ' + this.id;
		      
		  }


	      }

	  
	      decryptMessage(msg, step) {
		  var facts = msg.content.getFacts();
		  if (facts.length != 1) { 
		      console.log('Expecting exactly one argument, received: ' + facts.length);
		      return;
		  }
		 
		  this.learns(facts[0]);
		  this.decrypt(facts[0], 'KBS', step);
		  this.step = 0; 
	      }

	  }

	  class Server extends Agent {
	      constructor(id, facts, network) {
		  super(id, facts, network);
		  this.state = 0;
	      }
     
	      process (msg, step) {
		  switch (this.state) {
		  case 0:
		      this.sendEncryptedKey(msg, step); break;
		  default:
		      throw 'Unknown state for agent ' + this.id;
		  }
	      }

	      sendEncryptedKey(msg, step) {
		  var facts = msg.content.getFacts();
		  if (facts.length != 2) { 
		      console.log('Expecting exactly two arguments, received: ' + facts.length);
		      return;
		  }
		  
		  var A = facts[0];
		  var B = facts[1];
		  var KAS = 'K' + A + 'S';
		  var KBS = 'K' + B + 'S';
		  
		  
		  var new_msg = {source:this.id, destination:A,
				 content:this.encrypt(KBS, KAS, step)};
		  this.sendMessage(new_msg, step);
		  
	      }

	  }
	  
	  class Attacker extends Agent {
	      process (msg) {
		  this.learns(msg.content.getFacts()[0]);
	      }
	  }
	  
	  var s1 = new NetStack('message_list1');	  
	  var a1 = new Sender('A', [], s1);
	  var b1 = new Receiver('B', [], s1);
	  var v1 = new Server('S', [], s1);
	  var e1 = new Attacker('E', [], s1);
	  s1.registerAgents([a1, b1, v1, e1]);
	  s1.registerAttacker(e1);
	  var c1 = new Command(s1, e1, [a1, b1, e1, v1]);
	  a1.learns('s', 0);
	  a1.learns('KAS', 0);
	  a1.learns('A', 0);
	  a1.learns('B', 0);
	  b1.learns('A', 0);
	  b1.learns('B', 0);
	  b1.learns('KBS', 0);
	  e1.learns('E', 0);
	  e1.learns('A', 0);
	  e1.learns('B', 0);
	  e1.learns('fake', 0);
	  e1.learns('KES', 0);
	  v1.learns('KES', 0);
	  v1.learns('KAS', 0);
	  v1.learns('KBS', 0);
	  v1.learns('A', 0);
	  v1.learns('B', 0);
	  v1.learns('E', 0);
	  v1.learns('K', 0);
	  
	  a1.createDiv('agents_box1', 'agent');      
	  b1.createDiv('agents_box1', 'agent');
	  v1.createDiv('agents_box1', 'agent');
	  e1.createDiv('agents_box1', 'agent');

	  setCurrentStep(0);

</script>


## Attacks


1. Can you find a sequence of commands such that E knows s_1? 
* {{< tip "Tip 1" tip1 >}}
We assume that E is registered with S, which means that E can do everything A does. 
{{< /tip >}}

* {{< tip "Tip 2" tip2 >}}
What happens if the server S receives the initial message "E, B"? 
{{< /tip >}}

* {{< tip "Tip 3" tip3 >}}
You also need A to sends the actual secret. 
{{< /tip >}}

* {{< tip Solution sol1 >}}
<span class='cmd'>
new_session();
transmit(0);
transmit(1);
intercept(2);
inject(E->S:E, B);
transmit(3);
transmit(4);
decrypt({KBS}KES, KES);
decrypt({s_1}KBS, KBS);
</span>
{{< /tip >}}


2. Can you find a sequence of commands such that B knows fake? 

* {{< tip Tip tip4 >}}
B is expecting something encrypted with KBS, you can use part of the previous attack to get that key. 
{{< /tip >}}


* {{< tip Tip tip5 >}}
A does not actually have to get involved for that attack to work. 
{{< /tip >}}



* {{< tip Solution sol2 >}}
<span class='cmd'>
inject(E->S:E, B);
transmit(0);
transmit(1);
decrypt({KBS}KES, KES);
encrypt(fake, KBS);
inject(A->B:{fake}KBS);
transmit(2);
</span>
{{< /tip >}}



{{< protocolgame_svg >}}
