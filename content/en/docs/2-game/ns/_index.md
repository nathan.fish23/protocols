 ---
title: "Needham Schroeder"
date: 2020-02-16
weight: 70
---
{{< pg_start >}}


We now introduce a simplified version of the original symmetric key Needham-Schroeder protocol, which uses nonces and explicit identities for the sender and the recipient. 

<b>Knowledge:</b> A:[s, KAS, A, B, NA], B:[KBS, A, B], S:[A, B, E, KAS, KBS, KES, K] E:[A, B, NE,  KES, fake]<br>
<b>Steps:</b>
  <ol>
    <li> A -> S: A, B, #NA
    <li> S -> A: {#NA, #K, B, {K, A}KBS}KAS
    <li> A -> B: {K, A}KBS, {#s}K
  </ol>

This protocol should resist against confidentiality attacks (i.e., E should not be able to learn the current session secret).
However, this protocol is still vulnerable against integrity attacks (i.e., E is still able to send a fake message spoofing A). 
	  
{{< protocolgame >}}

<script>	  
class Sender extends Agent {
	      
	      constructor(id, facts, network) {
		  super(id, facts, network);
		  this.session = 0;
		  this.state = 0;
		  this.currentKey = null;

	      }
	      
	      // Sender has the following states
	      // init: initial state, ready to send message 1
	      // (in case of multiple sessions, message 3 from previous session
	      // has been sent)
	      // waiting_2: message 1 sent, waiting for message 2. 
	      
	      process (msg, step) {
		  switch (this.state) {
		  case 0:
		      this.init(step); break;
		  case 1:
		      this.sendEncryptedSecret(msg, step); break;
		  default:
		      throw 'Unknown state for agent ' + this.id;
		  }
	      }
	      
	      getSessionSecret (step) {
		  var secret = 's_' + this.session;
		  this.learns(secret, step)
		  return secret;
	      }

	      getNonce (step) {
		  var nonce = 'NA_' + this.session;
		  this.learns(nonce, step);
		  return nonce;
	      }
	      
	      sendEncryptedSecret(msg, step) {
		  var facts = msg.content.getFacts();
		  if (facts.length != 1) { 
		      console.log('Expecting exactly one argument, received: ' + facts.length);
		      return;
		  }
		  
		  var KBS_KAS = facts[0];
		  this.learns(KBS_KAS, step);

		  var facts2 = this.decrypt(KBS_KAS, 'KAS', step).getFacts();
		  if (facts2.length != 4) { 
		      console.log('Expecting exactly three inner messages, received: ' + facts.length);
		      return;
		  }
		  var NA = facts2[0];
		  if (NA != this.getNonce()) {
		      throw 'Wrong nonce received: ' + NA + ' (expecting ' + this.getNonce() + ')';
		  }
		      
		  var K = facts2[1];
		  this.currentKey = K;

		  var B = facts2[2];
		  if (B != 'B') {
		      throw 'Wrong sender received: ' + B + ' (expecting B)'; 
		  }
		  

		  var K_KBS = facts2[3];

		  var S_K = this.encrypt(this.getSessionSecret(step), K);
		  var new_msg = {source:this.id, destination:'B',
				 content:K_KBS + ', ' +S_K};
		  this.sendMessage(new_msg, step);
		  this.state = 0;
		  
	      }
	      
	      init (step) {
		  this.session++;
		  this.getSessionSecret(step);
		  this.network.pastKeys.push(this.currentKey);
		  var msg = {source:this.id, destination:'S', content:'A, B, ' + this.getNonce()};
		  this.sendMessage(msg, step);
		  this.state = 1;
	      }
	  }

	  class Receiver extends Agent {
	      constructor(id, facts, network) {
		  super(id, facts, network);
		  this.state = 0;
	      }

	      process(msg, step) {
		  switch (this.state) {
		  case 0:
		      this.decryptMessage(msg, step); break;
		  default:
		      throw 'Unknown state for agent ' + this.id;
		      
		  }


	      }

	      decryptMessage(msg, step) {
		  var facts = msg.content.getFacts();
		  if (facts.length != 2) { 
		      throw (this.id + ': Expecting exactly two arguments, received: ' + facts.length);
		      return;
		  }

		  var KA_KBS = facts[0];
		  var S_K = facts[1];
		  this.learns(KA_KBS);
		  this.learns(S_K);

		  var facts2 = this.decrypt(KA_KBS, 'KBS', step).getFacts();
		  if (facts2.length != 2) { 
		      console.log('Expecting exactly three inner messages, received: ' + facts.length);
		      return;
		  }

		  var K = facts2[0];
		  var A = facts2[1];

		  if (A != 'A') {
		      throw 'Wrong recipient received: ' + A + ' (expecting A)'; 
		  }

		  
		  var S = this.decrypt(S_K, K, step);
		  
		  this.step = 0; 
	      }

	  }

	  class Server extends Agent {
	      constructor(id, facts, network) {
		  super(id, facts, network);
		  this.state = 0;
		  this.key = null;
		  this.session = 0;
	      }

	      
	      getFreshKey(step) {
		  var k = 'K_' + this.session++;
		  this.learns(k, step)
		  this.key = k;
		  return k;
	      }

	      process (msg, step) {
		  switch (this.state) {
		  case 0:
		      this.sendEncryptedKey(msg, step); break;
		  default:
		      throw 'Unknown state for agent ' + this.id;
		  }
	      }
	      
	      sendEncryptedKey(msg, step) {
		  var facts = msg.content.getFacts();
		  if (facts.length != 3) { 
		      console.log('Expecting exactly two arguments, received: ' + facts.length);
		      return;
		  }
		  
		  var A = facts[0];
		  var B = facts[1];
		  var NA = facts[2];
		  var K = this.getFreshKey(step);
		  this.learns(K, step);
		  this.learns(NA);
		  
		  var KAS = 'K' + A + 'S';
		  var KBS = 'K' + B + 'S';

		  var K_KBS = this.encryptList([K, A], KBS, step);
		  
		  var new_msg = {source:this.id, destination:A,
				 content:this.encryptList([NA, K, B, K_KBS], KAS, step)};
		  this.sendMessage(new_msg, step);
		  
	      }

	  }
	  
	  class Attacker extends Agent {
	      process (msg) {
		  var facts = msg.content.getFacts();
		  for (var i = 0; i < facts.length; i++)
		      this.learns(facts[i]);
	      }

	      
	      compromise(key) {
		  if (this.network.pastKeys.includes(key)) {
		      this.learns(key);
		  }
		  else
		      throw key + ' is not a past key, cannot be compromised';
	      }

	  }
	  
	  var s1 = new NetStack('message_list1');	  
	  var a1 = new Sender('A', ['s', 'KAS', 'A', 'B', 'NA'], s1);
	  var b1 = new Receiver('B', ['A', 'B', 'KBS'], s1);
	  var v1 = new Server('S', ['KES', 'KAS', 'KBS', 'A', 'B', 'E'], s1);
	  var e1 = new Attacker('E', ['E', 'A', 'B', 'NE', 'fake', 'KES'], s1);
	  s1.registerAgents([a1, b1, v1, e1]);
	  s1.registerAttacker(e1);
	  var c1 = new Command(s1, e1, [a1, b1, e1, v1]);
	  
	  a1.createDiv('agents_box1', 'agent');      
	  b1.createDiv('agents_box1', 'agent');
	  v1.createDiv('agents_box1', 'agent');
	  e1.createDiv('agents_box1', 'agent');

	  setCurrentStep(0);
	  

	  

</script>


## Attacks


1. Can you find a sequence of commands such that E knows the session secret? 
* {{< tip "Tip 1" tip1 >}}
You can try all the attacks covered so far. 
{{< /tip >}}


* {{< tip Solution sol1 >}}
It should not be possible for E to know the session secret. 
{{< /tip >}}


2. Can you find a sequence of commands such that B knows fake? 

* {{< tip Tip tip4 >}}
Are the conditions for the previous attack still there? 
{{< /tip >}}

* {{< tip Solution sol2 >}}
<span class='cmd'>
new_session();
transmit(0);
transmit(1);
intercept(2);
new_session();
compromise(K_0);
encrypt(fake, K_0);
inject(A->B:{K_0, A}KBS, {fake}K_0);
transmit(4);
</span>
{{< /tip >}}



{{< protocolgame_svg >}}
