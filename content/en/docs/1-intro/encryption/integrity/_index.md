---
title: "Integrity"
date: 2020-08-21
weight: 20
---
{{< pg_start >}}



Let us consider the case where she tries to send a fake message to Bob, and we add "fake" and "K2" to her 
initial knowledge.

<div class='game'>
    <div class='agents' id='agents_box'></div>
    <div class='network' id='bar'>
        <center>Network stack</center>
        <ul id='message_list'></ul>
    </div>
</div>


<div class='steps'>
    <button onclick="step20();">Step 0</button>
    <button onclick="step21();">Step 1 (A sends message)</button>
    <button onclick="step22();">Step 2 (E intercepts message 0)</button>
    <button onclick="step23();">Step 3 (E blocks message 0)</button>
    <button onclick="step24();">Step 4 (E sends message 1)</button>
    <button onclick="step25();">Step 5 (E transmits message 1)</button>
</div>
  
<script>
    class Listener extends Agent {
	process (msg, step) {
	    var facts = msg.content.getFacts();
	    for (var i = 0; i < facts.length; i++) {
		this.learns(facts[i], step);
	    }
	}
    }
    
    var s2 = new NetStack('message_list');
    var a2 = new Agent('A', ['secret', 'K', '{secret}K'], s2);
    var b2 = new Listener('B', ['K'], s2);
    var e2 = new Listener('E', ['K2', 'fake', '{fake}K2'], s2);
    s2.registerAttacker(e2);
    s2.registerAgents([a2, b2, e2]);
    
    a2.createDiv('agents_box', 'agent');      
    b2.createDiv('agents_box', 'agent');
    e2.createDiv('agents_box', 'adversary');

    setCurrentStep(0);

    function step20 () {
        clearBox(0, 'box2');
        s2.clearStack(0);
        s2.refreshStack();
        b2.forgets(0);
        b2.updateContent();
        setCurrentStep(0);
    }
    
    function step21 () {
        step20();
        clearBox(1, 'box2');
        s2.addMessage(create_message('A -> B: {secret}K'), 1, 'w', 'box2');
        s2.refreshStack();
        setCurrentStep(2);	
    
    }
    function step22 () {
        step21();
        clearBox(2, 'box2');
        s2.interceptMessage(0, 2, 'box2');
        s2.refreshStack();
    
        e2.updateContent();
        setCurrentStep(2);	
    }
    
    function step23 () {
        step22();
        s2.blockMessage(0, 3);
        s2.refreshStack();
        setCurrentStep(3);	
    
    }
    
    function step24 () {
        step23();
        s2.injectMessage(e2, create_message('A -> B: {fake}K2'), 1, 'w', 'box2');
        s2.refreshStack();
        setCurrentStep(4);	
    
    }
    
    
    function step25 () {
    step24();
        
        s2.transmitMessage(1, 5, 'box2');
        b2.updateContent();
        s2.refreshStack();
        setCurrentStep(5);	
    
    }
    
</script>


As previously done, we automatically added "{fake}K2" to Eve's knowledge. However, even though Bob knows "{fake}K2", 
he does not know "fake": if he tries to decrypt "{fake}K2" with the key "K", he would not get "fake" 
(we consider here that Eve wants Bob to know a precise message "fake", not some random text). It is also important 
that even if Bob knew "K2", he would try to decrypt with the key "K" as it is what is expected from the protocol. 




{{< protocolgame_svg >}}
