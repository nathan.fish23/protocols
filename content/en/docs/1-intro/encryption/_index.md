---
title: "Encryption"
date: 2020-02-17
weight: 40
---
{{< protocolgame_start >}}


In the previous examples, all messages were sent in plain-text, meaning that Eve could intercept all of them, 
and know their content. The traditional way to avoid this is to use encryption: given a message "m" and a key "K", 
the message "{m}K" is a cipher-text corresponding to "m" encrypted by "K". We first consider the case of symmetric 
encryption: only agents who knows the key "K" can encrypt and decrypt using that key.
