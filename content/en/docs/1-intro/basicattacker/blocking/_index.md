---
title: "Blocking"
date: 2020-02-17
weight: 20
---
{{< pg_start >}}


  
### Blocking
  
Eve can also block any message on the network. For instance, she can block the message "secret" from Alice to Bob.

We now introduce the status "/b", which indicates that the message has been blocked (and the message should be displayed in red). 


<div class='game'>
    <div class='agents' id='agents_box'></div>
    <div class='network' id='bar'>
        <center>Network stack</center>
        <ul id='message_list'></ul>
    </div>
</div>

  <div class='steps'>
    <button onclick="step20();">Step 0</button>
    <button onclick="step21();">Step 1 (A sends message)</button>
    <button onclick="step22();">Step 2 (message blocked)</button>
  </div>

<script>
class Listener extends Agent {
    process (msg, step) {
	    var facts = msg.content.getFacts();
	    for (var i = 0; i < facts.length; i++) {
	        this.learns(facts[i], step);
	    }
    }
}

let s = new NetStack('message_list');
let a = new Agent('A', ['secret'], s);
let b = new Listener('B', [], s);
let e = new Listener('E', [], s);
s.registerAgents([a, b, e]);
s.registerAttacker(e);

a.createDiv('agents_box', 'agent');      
b.createDiv('agents_box', 'agent');
e.createDiv('agents_box', 'adversary');

setCurrentStep(0);

function step20 () {
    clearBox(0, 'box');
    s.clearStack(0);
    s.refreshStack();	      
    setCurrentStep(0);
    
}

function step21 () {
    step20();
    clearBox(1, 'box');
    s.addMessage(create_message('A -> B: secret'), 1, 'w');
    s.refreshStack();
    setCurrentStep(1);	
    
}

function step22 () {
    step21();
    clearBox(0, 'box');
    s.blockMessage(0, 2);
    s.refreshStack();	      
    setCurrentStep(2);
}
</script>

  <p>
    

  {{< protocolgame_svg >}}
