---
title: "Man in the middle"
date: 2020-02-17
weight: 40
---
{{< pg_start >}}



### Man in the middle attack 

Finally, Eve can combine all the attacks: she can intercept "secret" from Alice, blocks it so that it does not arrive 
to Bob, and send a fake message instead. This is a classical example of a <b>Man-in-the-middle attack</b>.


<div class='game'>
    <div class='agents' id='agents_box'></div>
    <div class='network' id='bar'>
        <center>Network stack</center>
        <ul id='message_list'></ul>
    </div>
</div>

<div class='steps'>
    <button onclick="step50();">Step 0</button>
    <button onclick="step51();">Step 1 (A sends message 0)</button>
    <button onclick="step52();">Step 2 (E intercepts message 0)</button>
    <button onclick="step53();">Step 3 (message 0 blocked)</button>
    <button onclick="step54();">Step 4 (E sends message 1)</button>
    <button onclick="step55();">Step 5 (message 1 transmitted)</button>
</div>
  
<script>

    class Listener extends Agent {
        process (msg, step) {
    	    var facts = msg.content.getFacts();
    	    for (var i = 0; i < facts.length; i++) {
    	        this.learns(facts[i], step);
    	    }
        }
    }
    
    let s = new NetStack('message_list');
    let a = new Agent('A', ['secret'], s);
    let b = new Listener('B', [], s);
    let e = new Listener('E', ['fake'], s);
    s.registerAttacker(e);
    s.registerAgents([a, b, e]);

    a.createDiv('agents_box', 'agent');      
    b.createDiv('agents_box', 'agent');
    e.createDiv('agents_box', 'adversary');

    setCurrentStep(0);
    
    function step50 () {
        clearBox(0, 'box');
        s.clearStack(0);
        s.refreshStack();
        b.forgets(0);
        b.updateContent();
        e.forgets(0);
        e.updateContent();
        setCurrentStep(0);
    }
    
    function step51 () {
        step50();
        clearBox(1, 'box');
        s.addMessage(create_message('A -> B: secret'), 1, 'w');
        s.refreshStack();
        setCurrentStep(1);	
    }
    
    function step52 () {
        step51();
        clearBox(2, 'box');
        e.updateContent();
        s.interceptMessage(0, 2);
        s.refreshStack();
        setCurrentStep(2);    
    }

    function step53() {
        step52();
        clearBox(3, 'box');
        s.blockMessage(0, 3);
        s.refreshStack();
        setCurrentStep(3);
    }

    function step54 () {
        step53();
        clearBox(4, 'box')
        s.injectMessage(e, create_message('A -> B: fake'), 4, 'w', 'box')
        s.refreshStack();
    
        setCurrentStep(4);	
    
    }

    function step55 () {
        step54();
        clearBox(5, 'box')
        s.transmitMessage(1, 5);
        s.refreshStack();
        b.updateContent();
        setCurrentStep(5);	
    }

  </script>


  {{< protocolgame_svg >}}
